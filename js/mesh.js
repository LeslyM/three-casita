// mesh ////////////////////////////////////////////////////  

function createFigure() {

    //plane
    const geometry_plane = new THREE.PlaneGeometry(50, 50, 32);
    const material_3 = new THREE.MeshPhongMaterial({ color: "rgb(157, 219, 26)" });
    const plane = new THREE.Mesh(geometry_plane, material_3);
    plane.rotation.x = Math.PI * -.5;
    scene.add(plane);


    house();

    tree(6, -10, 5);
    tree(4, 10, 8);
    tree(5, 8, -10);
    tree(3, -8, -10);
}


function house() {
    //cubo
    const cubeSize = 7;
    geometry_cube = new THREE.BoxGeometry(cubeSize, cubeSize, cubeSize);
    material_1 = new THREE.MeshPhongMaterial({ color: "rgb(252, 224, 96)" });
    cube = new THREE.Mesh(geometry_cube, material_1);
    cube.position.set(0, cubeSize / 2, 0);
    scene.add(cube);

    //cono
    const coneSize = 8;
    geometry_cone = new THREE.ConeGeometry(coneSize / 1.4, coneSize / 2, 1000);
    material_cone = new THREE.MeshBasicMaterial({ color: "rgb(102, 84, 31)" });
    cone = new THREE.Mesh(geometry_cone, material_cone);
    cone.position.set(0, cubeSize * 1.29, 0);
    scene.add(cone);

    //puerta
    geometry_door = new THREE.BoxGeometry(3, 4, 0.2);
    material_door = new THREE.MeshPhongMaterial({ color: "rgb(102, 84, 31)" });
    door = new THREE.Mesh(geometry_door, material_door);
    door.position.set(0, 1.9, -cubeSize + 3.5);
    scene.add(door);

    //handle
    const sphereRadius = 0.3;
    geometry_sphere = new THREE.SphereGeometry(sphereRadius, 32, 32);
    material_2 = new THREE.MeshPhongMaterial({ color: "rgb(147, 230, 197)" });
    sphere = new THREE.Mesh(geometry_sphere, material_2);
    sphere.position.set(0.7, sphereRadius + 1.5, -cubeSize + 3.5);
    scene.add(sphere);

    //window1
    geometry_window_1 = new THREE.BoxGeometry(0.2, 3, 4);
    material_window_1 = new THREE.MeshPhongMaterial({ color: "rgb(147, 230, 197)" });
    window_1 = new THREE.Mesh(geometry_window_1, material_window_1);
    window_1.position.set(-cubeSize + 3.5, 3, 0);
    scene.add(window_1);

    //window2
    geometry_window_2 = new THREE.BoxGeometry(0.2, 3, 4);
    material_window_2 = new THREE.MeshPhongMaterial({ color: "rgb(147, 230, 197)" });
    window_2 = new THREE.Mesh(geometry_window_2, material_window_2);
    window_2.position.set(cubeSize - 3.5, 3, 0);
    scene.add(window_2);

}

function tree(scale, pos_x, pos_z) {

    geometry_cone = new THREE.ConeGeometry(scale / 3, scale, scale);
    material_cone = new THREE.MeshBasicMaterial({ color: "rgb(39, 92, 4)" });
    cone = new THREE.Mesh(geometry_cone, material_cone);
    cone.position.set(pos_x, scale, pos_z);
    scene.add(cone);

    geometry_cylinder = new THREE.CylinderGeometry(scale / 7, scale / 7, scale / 1.5, 1000);
    material_cylinder = new THREE.MeshBasicMaterial({ color: "rgb(92, 66, 11)" });
    cylinder = new THREE.Mesh(geometry_cylinder, material_cylinder);
    cylinder.position.set(pos_x, scale / 3, pos_z);
    scene.add(cylinder);
}