function ligth() {


    // spotLight  ///////////////////////////////////////

    const spotLight = new THREE.SpotLight(0xffffff);
    spotLight.position.set(20, 20, -30);
    spotLight.target.position.set(0, 0, 0);
    scene.add(spotLight);
    scene.add(spotLight.target);

    const helper = new THREE.DirectionalLightHelper(spotLight);
    scene.add(helper);

    function updateLight() {
        spotLight.target.updateMatrixWorld();
        helper.update();
    }
    updateLight();
    // gui
    const gui = new dat.GUI();
    gui.addColor(new ColorGUIHelper(spotLight, 'color'), 'value').name('color');
    gui.add(spotLight, 'intensity', 0, 2, 0.01);
    gui.add(spotLight, 'distance', 0, 40).onChange(updateLight);
    gui.add(new DegRadHelper(spotLight, 'angle'), 'value', 0, 90).name('angle').onChange(updateLight);
    gui.add(spotLight, 'penumbra', 0, 1, 0.01);

    makeXYZGUI(gui, spotLight.position, 'position', updateLight);
    makeXYZGUI(gui, spotLight.target.position, 'target', updateLight);
}