// camera //////////////////////////////////////////////////
var aspect = window.innerWidth / window.innerHeight;
var camera = new THREE.PerspectiveCamera(75, aspect);
camera.position.set(0, 10, 20);

// scene //////////////////////////////////////////////////
var scene = new THREE.Scene();
scene.backgroundColor = new THREE.Color("rgb(255, 0, 0)");

createFigure();
ligth();

//////////////////////////////////////////////////////////// 
////////////////////////////////////////////////////////////

// renderer //////////////////////////////////////////////////
var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
renderer.render(scene, camera);

// OrbitControls /////////////////////////////////////////////
var controls = new THREE.OrbitControls(camera, renderer.domElement);

// axes ///////////////////////////////////////////////////////
const axesHelper = new THREE.AxesHelper(15);
scene.add(axesHelper);

// animation //////////////////////////////////////////////////
var animate = function() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
}
animate();

// redimensioar  /////////////////////////////////////////////
window.addEventListener('resize', redimensionar);

function redimensionar() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.render(scene, camera);
}